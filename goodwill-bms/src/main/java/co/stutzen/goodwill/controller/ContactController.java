package co.stutzen.goodwill.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.stutzen.webframework.controller.SWFController;
import org.stutzen.webframework.service.SWFService;

import co.stutzen.goodwill.entity.Contact;
import co.stutzen.goodwill.service.ContactService;

@Controller
@RequestMapping("/contact")
public class ContactController extends SWFController<Contact> {

	@Autowired
	private ContactService contactService;

	@Autowired
	public ContactController(SWFService<Contact> swfService) {
		super(swfService);

	}

}
