package co.stutzen.goodwill.mapper;

import java.util.List;

import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.JdbcType;

import co.stutzen.goodwill.entity.BillDetail;
import co.stutzen.goodwill.entity.BillDetailExample;

@CacheNamespace(
implementation=org.mybatis.caches.ehcache.LoggingEhcache.class,eviction=org.apache.ibatis.cache.decorators.LruCache.class,flushInterval=100,size=100
)
public interface BillDetailMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@SelectProvider(type = BillDetailSqlProvider.class, method = "countByExample")
	@Options(useCache = false, flushCache = true)
	int countByExample(BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@DeleteProvider(type = BillDetailSqlProvider.class, method = "deleteByExample")
	@Options(useCache = false, flushCache = true)
	int deleteByExample(BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@Delete({ "delete from tbl_bill_details", "where id = #{id,jdbcType=INTEGER}" })
	@Options(useCache = false, flushCache = true)
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@Insert({ "insert into tbl_bill_details (bill_id, sku, ", "item_id, item_name, ",
			"ordered_quantity, billed_quantity, ", "uom_id, uom_value, ", "mrp_price, selling_price, ",
			"total_price, created_by, ", "modified_by, created_on, ", "lastmodifiedon, is_active, ", "comments)",
			"values (#{billId,jdbcType=INTEGER}, #{sku,jdbcType=VARCHAR}, ",
			"#{itemId,jdbcType=INTEGER}, #{itemName,jdbcType=VARCHAR}, ",
			"#{orderedQty,jdbcType=INTEGER}, #{billedQty,jdbcType=INTEGER}, ",
			"#{uomId,jdbcType=INTEGER}, #{uomValue,jdbcType=VARCHAR}, ",
			"#{mrpPrice,jdbcType=DECIMAL}, #{sellingPrice,jdbcType=DECIMAL}, ",
			"#{totalPrice,jdbcType=DECIMAL}, #{createdby,jdbcType=INTEGER}, ",
			"#{lastmodifiedby,jdbcType=INTEGER}, #{createdon,jdbcType=TIMESTAMP}, ",
			"#{lastmodifiedon,jdbcType=TIMESTAMP}, #{isActive,jdbcType=BIT}, ", "#{comments,jdbcType=LONGVARCHAR})" })
	@SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
	@Options(useCache = false, flushCache = true)
	int insert(BillDetail record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@InsertProvider(type = BillDetailSqlProvider.class, method = "insertSelective")
	@SelectKey(statement = "SELECT LAST_INSERT_ID()", keyProperty = "id", before = false, resultType = Integer.class)
	@Options(useCache = false, flushCache = true)
	int insertSelective(BillDetail record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@SelectProvider(type = BillDetailSqlProvider.class, method = "selectByExampleWithBLOBs")
	@Results({ @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
			@Result(column = "bill_id", property = "billId", jdbcType = JdbcType.INTEGER),
			@Result(column = "sku", property = "sku", jdbcType = JdbcType.VARCHAR),
			@Result(column = "item_id", property = "itemId", jdbcType = JdbcType.INTEGER),
			@Result(column = "item_name", property = "itemName", jdbcType = JdbcType.VARCHAR),
			@Result(column = "ordered_quantity", property = "orderedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "billed_quantity", property = "billedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_id", property = "uomId", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_value", property = "uomValue", jdbcType = JdbcType.VARCHAR),
			@Result(column = "mrp_price", property = "mrpPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "selling_price", property = "sellingPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "total_price", property = "totalPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "created_by", property = "createdby", jdbcType = JdbcType.INTEGER),
			@Result(column = "modified_by", property = "lastmodifiedby", jdbcType = JdbcType.INTEGER),
			@Result(column = "created_on", property = "createdon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "lastmodifiedon", property = "lastmodifiedon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "is_active", property = "isActive", jdbcType = JdbcType.BIT),
			@Result(column = "comments", property = "comments", jdbcType = JdbcType.LONGVARCHAR) })
	@Options(useCache = true, flushCache = false)
	List<BillDetail> selectByExampleWithBLOBsWithRowbounds(BillDetailExample example, RowBounds rowBounds);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@SelectProvider(type = BillDetailSqlProvider.class, method = "selectByExampleWithBLOBs")
	@Results({ @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
			@Result(column = "bill_id", property = "billId", jdbcType = JdbcType.INTEGER),
			@Result(column = "sku", property = "sku", jdbcType = JdbcType.VARCHAR),
			@Result(column = "item_id", property = "itemId", jdbcType = JdbcType.INTEGER),
			@Result(column = "item_name", property = "itemName", jdbcType = JdbcType.VARCHAR),
			@Result(column = "ordered_quantity", property = "orderedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "billed_quantity", property = "billedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_id", property = "uomId", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_value", property = "uomValue", jdbcType = JdbcType.VARCHAR),
			@Result(column = "mrp_price", property = "mrpPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "selling_price", property = "sellingPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "total_price", property = "totalPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "created_by", property = "createdby", jdbcType = JdbcType.INTEGER),
			@Result(column = "modified_by", property = "lastmodifiedby", jdbcType = JdbcType.INTEGER),
			@Result(column = "created_on", property = "createdon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "lastmodifiedon", property = "lastmodifiedon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "is_active", property = "isActive", jdbcType = JdbcType.BIT),
			@Result(column = "comments", property = "comments", jdbcType = JdbcType.LONGVARCHAR) })
	@Options(useCache = true, flushCache = false)
	List<BillDetail> selectByExampleWithBLOBs(BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@SelectProvider(type = BillDetailSqlProvider.class, method = "selectByExample")
	@Results({ @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
			@Result(column = "bill_id", property = "billId", jdbcType = JdbcType.INTEGER),
			@Result(column = "sku", property = "sku", jdbcType = JdbcType.VARCHAR),
			@Result(column = "item_id", property = "itemId", jdbcType = JdbcType.INTEGER),
			@Result(column = "item_name", property = "itemName", jdbcType = JdbcType.VARCHAR),
			@Result(column = "ordered_quantity", property = "orderedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "billed_quantity", property = "billedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_id", property = "uomId", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_value", property = "uomValue", jdbcType = JdbcType.VARCHAR),
			@Result(column = "mrp_price", property = "mrpPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "selling_price", property = "sellingPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "total_price", property = "totalPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "created_by", property = "createdby", jdbcType = JdbcType.INTEGER),
			@Result(column = "modified_by", property = "lastmodifiedby", jdbcType = JdbcType.INTEGER),
			@Result(column = "created_on", property = "createdon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "lastmodifiedon", property = "lastmodifiedon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "is_active", property = "isActive", jdbcType = JdbcType.BIT) })
	@Options(useCache = true, flushCache = false)
	List<BillDetail> selectByExampleWithRowbounds(BillDetailExample example, RowBounds rowBounds);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@SelectProvider(type = BillDetailSqlProvider.class, method = "selectByExample")
	@Results({ @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
			@Result(column = "bill_id", property = "billId", jdbcType = JdbcType.INTEGER),
			@Result(column = "sku", property = "sku", jdbcType = JdbcType.VARCHAR),
			@Result(column = "item_id", property = "itemId", jdbcType = JdbcType.INTEGER),
			@Result(column = "item_name", property = "itemName", jdbcType = JdbcType.VARCHAR),
			@Result(column = "ordered_quantity", property = "orderedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "billed_quantity", property = "billedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_id", property = "uomId", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_value", property = "uomValue", jdbcType = JdbcType.VARCHAR),
			@Result(column = "mrp_price", property = "mrpPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "selling_price", property = "sellingPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "total_price", property = "totalPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "created_by", property = "createdby", jdbcType = JdbcType.INTEGER),
			@Result(column = "modified_by", property = "lastmodifiedby", jdbcType = JdbcType.INTEGER),
			@Result(column = "created_on", property = "createdon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "lastmodifiedon", property = "lastmodifiedon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "is_active", property = "isActive", jdbcType = JdbcType.BIT) })
	@Options(useCache = true, flushCache = false)
	List<BillDetail> selectByExample(BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@Select({ "select", "id, bill_id, sku, item_id, item_name, ordered_quantity, billed_quantity, uom_id, ",
			"uom_value, mrp_price, selling_price, total_price, created_by, modified_by, created_on, ",
			"lastmodifiedon, is_active, comments", "from tbl_bill_details", "where id = #{id,jdbcType=INTEGER}" })
	@Results({ @Result(column = "id", property = "id", jdbcType = JdbcType.INTEGER, id = true),
			@Result(column = "bill_id", property = "billId", jdbcType = JdbcType.INTEGER),
			@Result(column = "sku", property = "sku", jdbcType = JdbcType.VARCHAR),
			@Result(column = "item_id", property = "itemId", jdbcType = JdbcType.INTEGER),
			@Result(column = "item_name", property = "itemName", jdbcType = JdbcType.VARCHAR),
			@Result(column = "ordered_quantity", property = "orderedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "billed_quantity", property = "billedQty", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_id", property = "uomId", jdbcType = JdbcType.INTEGER),
			@Result(column = "uom_value", property = "uomValue", jdbcType = JdbcType.VARCHAR),
			@Result(column = "mrp_price", property = "mrpPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "selling_price", property = "sellingPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "total_price", property = "totalPrice", jdbcType = JdbcType.DECIMAL),
			@Result(column = "created_by", property = "createdby", jdbcType = JdbcType.INTEGER),
			@Result(column = "modified_by", property = "lastmodifiedby", jdbcType = JdbcType.INTEGER),
			@Result(column = "created_on", property = "createdon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "lastmodifiedon", property = "lastmodifiedon", jdbcType = JdbcType.TIMESTAMP),
			@Result(column = "is_active", property = "isActive", jdbcType = JdbcType.BIT),
			@Result(column = "comments", property = "comments", jdbcType = JdbcType.LONGVARCHAR) })
	@Options(useCache = true, flushCache = false)
	BillDetail selectByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@UpdateProvider(type = BillDetailSqlProvider.class, method = "updateByExampleSelective")
	@Options(useCache = false, flushCache = true)
	int updateByExampleSelective(@Param("record") BillDetail record, @Param("example") BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@UpdateProvider(type = BillDetailSqlProvider.class, method = "updateByExampleWithBLOBs")
	@Options(useCache = false, flushCache = true)
	int updateByExampleWithBLOBs(@Param("record") BillDetail record, @Param("example") BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@UpdateProvider(type = BillDetailSqlProvider.class, method = "updateByExample")
	@Options(useCache = false, flushCache = true)
	int updateByExample(@Param("record") BillDetail record, @Param("example") BillDetailExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@UpdateProvider(type = BillDetailSqlProvider.class, method = "updateByPrimaryKeySelective")
	@Options(useCache = false, flushCache = true)
	int updateByPrimaryKeySelective(BillDetail record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@Update({ "update tbl_bill_details", "set bill_id = #{billId,jdbcType=INTEGER},", "sku = #{sku,jdbcType=VARCHAR},",
			"item_id = #{itemId,jdbcType=INTEGER},", "item_name = #{itemName,jdbcType=VARCHAR},",
			"ordered_quantity = #{orderedQty,jdbcType=INTEGER},", "billed_quantity = #{billedQty,jdbcType=INTEGER},",
			"uom_id = #{uomId,jdbcType=INTEGER},", "uom_value = #{uomValue,jdbcType=VARCHAR},",
			"mrp_price = #{mrpPrice,jdbcType=DECIMAL},", "selling_price = #{sellingPrice,jdbcType=DECIMAL},",
			"total_price = #{totalPrice,jdbcType=DECIMAL},", "created_by = #{createdby,jdbcType=INTEGER},",
			"modified_by = #{lastmodifiedby,jdbcType=INTEGER},", "created_on = #{createdon,jdbcType=TIMESTAMP},",
			"lastmodifiedon = #{lastmodifiedon,jdbcType=TIMESTAMP},", "is_active = #{isActive,jdbcType=BIT},",
			"comments = #{comments,jdbcType=LONGVARCHAR}", "where id = #{id,jdbcType=INTEGER}" })
	@Options(useCache = false, flushCache = true)
	int updateByPrimaryKeyWithBLOBs(BillDetail record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_bill_details
	 * @mbggenerated
	 */
	@Update({ "update tbl_bill_details", "set bill_id = #{billId,jdbcType=INTEGER},", "sku = #{sku,jdbcType=VARCHAR},",
			"item_id = #{itemId,jdbcType=INTEGER},", "item_name = #{itemName,jdbcType=VARCHAR},",
			"ordered_quantity = #{orderedQty,jdbcType=INTEGER},", "billed_quantity = #{billedQty,jdbcType=INTEGER},",
			"uom_id = #{uomId,jdbcType=INTEGER},", "uom_value = #{uomValue,jdbcType=VARCHAR},",
			"mrp_price = #{mrpPrice,jdbcType=DECIMAL},", "selling_price = #{sellingPrice,jdbcType=DECIMAL},",
			"total_price = #{totalPrice,jdbcType=DECIMAL},", "created_by = #{createdby,jdbcType=INTEGER},",
			"modified_by = #{lastmodifiedby,jdbcType=INTEGER},", "created_on = #{createdon,jdbcType=TIMESTAMP},",
			"lastmodifiedon = #{lastmodifiedon,jdbcType=TIMESTAMP},", "is_active = #{isActive,jdbcType=BIT}",
			"where id = #{id,jdbcType=INTEGER}" })
	@Options(useCache = false, flushCache = true)
	int updateByPrimaryKey(BillDetail record);
}