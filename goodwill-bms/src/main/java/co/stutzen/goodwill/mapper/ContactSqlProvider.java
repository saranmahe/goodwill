package co.stutzen.goodwill.mapper;

import static org.apache.ibatis.jdbc.SqlBuilder.BEGIN;
import static org.apache.ibatis.jdbc.SqlBuilder.DELETE_FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.FROM;
import static org.apache.ibatis.jdbc.SqlBuilder.INSERT_INTO;
import static org.apache.ibatis.jdbc.SqlBuilder.ORDER_BY;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT;
import static org.apache.ibatis.jdbc.SqlBuilder.SELECT_DISTINCT;
import static org.apache.ibatis.jdbc.SqlBuilder.SET;
import static org.apache.ibatis.jdbc.SqlBuilder.SQL;
import static org.apache.ibatis.jdbc.SqlBuilder.UPDATE;
import static org.apache.ibatis.jdbc.SqlBuilder.VALUES;
import static org.apache.ibatis.jdbc.SqlBuilder.WHERE;

import co.stutzen.goodwill.entity.Contact;
import co.stutzen.goodwill.entity.ContactExample.Criteria;
import co.stutzen.goodwill.entity.ContactExample.Criterion;
import co.stutzen.goodwill.entity.ContactExample;
import java.util.List;
import java.util.Map;

public class ContactSqlProvider {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String countByExample(ContactExample example) {
		BEGIN();
		SELECT("count(*)");
		FROM("tbl_contact");
		applyWhere(example, false);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String deleteByExample(ContactExample example) {
		BEGIN();
		DELETE_FROM("tbl_contact");
		applyWhere(example, false);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String insertSelective(Contact record) {
		BEGIN();
		INSERT_INTO("tbl_contact");
		if (record.getName() != null) {
			VALUES("name", "#{name,jdbcType=VARCHAR}");
		}
		if (record.getEmail() != null) {
			VALUES("email", "#{email,jdbcType=VARCHAR}");
		}
		if (record.getMobile() != null) {
			VALUES("mobile", "#{mobile,jdbcType=VARCHAR}");
		}
		if (record.getTelephone() != null) {
			VALUES("telephone", "#{telephone,jdbcType=VARCHAR}");
		}
		if (record.getAddress() != null) {
			VALUES("address", "#{address,jdbcType=VARCHAR}");
		}
		if (record.getCountry() != null) {
			VALUES("country", "#{country,jdbcType=VARCHAR}");
		}
		if (record.getCity() != null) {
			VALUES("city", "#{city,jdbcType=VARCHAR}");
		}
		if (record.getPostcode() != null) {
			VALUES("postcode", "#{postcode,jdbcType=VARCHAR}");
		}
		if (record.getVat_id() != null) {
			VALUES("vat_id", "#{vat_id,jdbcType=VARCHAR}");
		}
		if (record.getCreatedby() != null) {
			VALUES("created_by", "#{createdby,jdbcType=INTEGER}");
		}
		if (record.getLastmodifiedby() != null) {
			VALUES("modified_by", "#{lastmodifiedby,jdbcType=INTEGER}");
		}
		if (record.getCreatedon() != null) {
			VALUES("created_on", "#{createdon,jdbcType=TIMESTAMP}");
		}
		if (record.getLastmodifiedon() != null) {
			VALUES("lastmodifiedon", "#{lastmodifiedon,jdbcType=TIMESTAMP}");
		}
		if (record.getComments() != null) {
			VALUES("comments", "#{comments,jdbcType=VARCHAR}");
		}
		if (record.getIsActive() != null) {
			VALUES("is_active", "#{isActive,jdbcType=BIT}");
		}
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String selectByExample(ContactExample example) {
		BEGIN();
		if (example != null && example.isDistinct()) {
			SELECT_DISTINCT("id");
		} else {
			SELECT("id");
		}
		SELECT("name");
		SELECT("email");
		SELECT("mobile");
		SELECT("telephone");
		SELECT("address");
		SELECT("country");
		SELECT("city");
		SELECT("postcode");
		SELECT("vat_id");
		SELECT("created_by");
		SELECT("modified_by");
		SELECT("created_on");
		SELECT("lastmodifiedon");
		SELECT("comments");
		SELECT("is_active");
		FROM("tbl_contact");
		applyWhere(example, false);
		if (example != null && example.getOrderByClause() != null) {
			ORDER_BY(example.getOrderByClause());
		}
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String updateByExampleSelective(Map<String, Object> parameter) {
		Contact record = (Contact) parameter.get("record");
		ContactExample example = (ContactExample) parameter.get("example");
		BEGIN();
		UPDATE("tbl_contact");
		if (record.getId() != null) {
			SET("id = #{record.id,jdbcType=INTEGER}");
		}
		if (record.getName() != null) {
			SET("name = #{record.name,jdbcType=VARCHAR}");
		}
		if (record.getEmail() != null) {
			SET("email = #{record.email,jdbcType=VARCHAR}");
		}
		if (record.getMobile() != null) {
			SET("mobile = #{record.mobile,jdbcType=VARCHAR}");
		}
		if (record.getTelephone() != null) {
			SET("telephone = #{record.telephone,jdbcType=VARCHAR}");
		}
		if (record.getAddress() != null) {
			SET("address = #{record.address,jdbcType=VARCHAR}");
		}
		if (record.getCountry() != null) {
			SET("country = #{record.country,jdbcType=VARCHAR}");
		}
		if (record.getCity() != null) {
			SET("city = #{record.city,jdbcType=VARCHAR}");
		}
		if (record.getPostcode() != null) {
			SET("postcode = #{record.postcode,jdbcType=VARCHAR}");
		}
		if (record.getVat_id() != null) {
			SET("vat_id = #{record.vat_id,jdbcType=VARCHAR}");
		}
		if (record.getCreatedby() != null) {
			SET("created_by = #{record.createdby,jdbcType=INTEGER}");
		}
		if (record.getLastmodifiedby() != null) {
			SET("modified_by = #{record.lastmodifiedby,jdbcType=INTEGER}");
		}
		if (record.getCreatedon() != null) {
			SET("created_on = #{record.createdon,jdbcType=TIMESTAMP}");
		}
		if (record.getLastmodifiedon() != null) {
			SET("lastmodifiedon = #{record.lastmodifiedon,jdbcType=TIMESTAMP}");
		}
		if (record.getComments() != null) {
			SET("comments = #{record.comments,jdbcType=VARCHAR}");
		}
		if (record.getIsActive() != null) {
			SET("is_active = #{record.isActive,jdbcType=BIT}");
		}
		applyWhere(example, true);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String updateByExample(Map<String, Object> parameter) {
		BEGIN();
		UPDATE("tbl_contact");
		SET("id = #{record.id,jdbcType=INTEGER}");
		SET("name = #{record.name,jdbcType=VARCHAR}");
		SET("email = #{record.email,jdbcType=VARCHAR}");
		SET("mobile = #{record.mobile,jdbcType=VARCHAR}");
		SET("telephone = #{record.telephone,jdbcType=VARCHAR}");
		SET("address = #{record.address,jdbcType=VARCHAR}");
		SET("country = #{record.country,jdbcType=VARCHAR}");
		SET("city = #{record.city,jdbcType=VARCHAR}");
		SET("postcode = #{record.postcode,jdbcType=VARCHAR}");
		SET("vat_id = #{record.vat_id,jdbcType=VARCHAR}");
		SET("created_by = #{record.createdby,jdbcType=INTEGER}");
		SET("modified_by = #{record.lastmodifiedby,jdbcType=INTEGER}");
		SET("created_on = #{record.createdon,jdbcType=TIMESTAMP}");
		SET("lastmodifiedon = #{record.lastmodifiedon,jdbcType=TIMESTAMP}");
		SET("comments = #{record.comments,jdbcType=VARCHAR}");
		SET("is_active = #{record.isActive,jdbcType=BIT}");
		ContactExample example = (ContactExample) parameter.get("example");
		applyWhere(example, true);
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	public String updateByPrimaryKeySelective(Contact record) {
		BEGIN();
		UPDATE("tbl_contact");
		if (record.getName() != null) {
			SET("name = #{name,jdbcType=VARCHAR}");
		}
		if (record.getEmail() != null) {
			SET("email = #{email,jdbcType=VARCHAR}");
		}
		if (record.getMobile() != null) {
			SET("mobile = #{mobile,jdbcType=VARCHAR}");
		}
		if (record.getTelephone() != null) {
			SET("telephone = #{telephone,jdbcType=VARCHAR}");
		}
		if (record.getAddress() != null) {
			SET("address = #{address,jdbcType=VARCHAR}");
		}
		if (record.getCountry() != null) {
			SET("country = #{country,jdbcType=VARCHAR}");
		}
		if (record.getCity() != null) {
			SET("city = #{city,jdbcType=VARCHAR}");
		}
		if (record.getPostcode() != null) {
			SET("postcode = #{postcode,jdbcType=VARCHAR}");
		}
		if (record.getVat_id() != null) {
			SET("vat_id = #{vat_id,jdbcType=VARCHAR}");
		}
		if (record.getCreatedby() != null) {
			SET("created_by = #{createdby,jdbcType=INTEGER}");
		}
		if (record.getLastmodifiedby() != null) {
			SET("modified_by = #{lastmodifiedby,jdbcType=INTEGER}");
		}
		if (record.getCreatedon() != null) {
			SET("created_on = #{createdon,jdbcType=TIMESTAMP}");
		}
		if (record.getLastmodifiedon() != null) {
			SET("lastmodifiedon = #{lastmodifiedon,jdbcType=TIMESTAMP}");
		}
		if (record.getComments() != null) {
			SET("comments = #{comments,jdbcType=VARCHAR}");
		}
		if (record.getIsActive() != null) {
			SET("is_active = #{isActive,jdbcType=BIT}");
		}
		WHERE("id = #{id,jdbcType=INTEGER}");
		return SQL();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_contact
	 * @mbggenerated
	 */
	protected void applyWhere(ContactExample example, boolean includeExamplePhrase) {
		if (example == null) {
			return;
		}
		String parmPhrase1;
		String parmPhrase1_th;
		String parmPhrase2;
		String parmPhrase2_th;
		String parmPhrase3;
		String parmPhrase3_th;
		if (includeExamplePhrase) {
			parmPhrase1 = "%s #{example.oredCriteria[%d].allCriteria[%d].value}";
			parmPhrase1_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
			parmPhrase2 = "%s #{example.oredCriteria[%d].allCriteria[%d].value} and #{example.oredCriteria[%d].criteria[%d].secondValue}";
			parmPhrase2_th = "%s #{example.oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{example.oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
			parmPhrase3 = "#{example.oredCriteria[%d].allCriteria[%d].value[%d]}";
			parmPhrase3_th = "#{example.oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
		} else {
			parmPhrase1 = "%s #{oredCriteria[%d].allCriteria[%d].value}";
			parmPhrase1_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s}";
			parmPhrase2 = "%s #{oredCriteria[%d].allCriteria[%d].value} and #{oredCriteria[%d].criteria[%d].secondValue}";
			parmPhrase2_th = "%s #{oredCriteria[%d].allCriteria[%d].value,typeHandler=%s} and #{oredCriteria[%d].criteria[%d].secondValue,typeHandler=%s}";
			parmPhrase3 = "#{oredCriteria[%d].allCriteria[%d].value[%d]}";
			parmPhrase3_th = "#{oredCriteria[%d].allCriteria[%d].value[%d],typeHandler=%s}";
		}
		StringBuilder sb = new StringBuilder();
		List<Criteria> oredCriteria = example.getOredCriteria();
		boolean firstCriteria = true;
		for (int i = 0; i < oredCriteria.size(); i++) {
			Criteria criteria = oredCriteria.get(i);
			if (criteria.isValid()) {
				if (firstCriteria) {
					firstCriteria = false;
				} else {
					sb.append(" or ");
				}
				sb.append('(');
				List<Criterion> criterions = criteria.getAllCriteria();
				boolean firstCriterion = true;
				for (int j = 0; j < criterions.size(); j++) {
					Criterion criterion = criterions.get(j);
					if (firstCriterion) {
						firstCriterion = false;
					} else {
						sb.append(" and ");
					}
					if (criterion.isNoValue()) {
						sb.append(criterion.getCondition());
					} else if (criterion.isSingleValue()) {
						if (criterion.getTypeHandler() == null) {
							sb.append(String.format(parmPhrase1, criterion.getCondition(), i, j));
						} else {
							sb.append(String.format(parmPhrase1_th, criterion.getCondition(), i, j,
									criterion.getTypeHandler()));
						}
					} else if (criterion.isBetweenValue()) {
						if (criterion.getTypeHandler() == null) {
							sb.append(String.format(parmPhrase2, criterion.getCondition(), i, j, i, j));
						} else {
							sb.append(String.format(parmPhrase2_th, criterion.getCondition(), i, j,
									criterion.getTypeHandler(), i, j, criterion.getTypeHandler()));
						}
					} else if (criterion.isListValue()) {
						sb.append(criterion.getCondition());
						sb.append(" (");
						List<?> listItems = (List<?>) criterion.getValue();
						boolean comma = false;
						for (int k = 0; k < listItems.size(); k++) {
							if (comma) {
								sb.append(", ");
							} else {
								comma = true;
							}
							if (criterion.getTypeHandler() == null) {
								sb.append(String.format(parmPhrase3, i, j, k));
							} else {
								sb.append(String.format(parmPhrase3_th, i, j, k, criterion.getTypeHandler()));
							}
						}
						sb.append(')');
					}
				}
				sb.append(')');
			}
		}
		if (sb.length() > 0) {
			WHERE(sb.toString());
		}
	}
}