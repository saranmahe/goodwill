package co.stutzen.goodwill.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDetailsExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public OrderDetailsExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andOrderIdIsNull() {
			addCriterion("order_id is null");
			return (Criteria) this;
		}

		public Criteria andOrderIdIsNotNull() {
			addCriterion("order_id is not null");
			return (Criteria) this;
		}

		public Criteria andOrderIdEqualTo(Integer value) {
			addCriterion("order_id =", value, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdNotEqualTo(Integer value) {
			addCriterion("order_id <>", value, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdGreaterThan(Integer value) {
			addCriterion("order_id >", value, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("order_id >=", value, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdLessThan(Integer value) {
			addCriterion("order_id <", value, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
			addCriterion("order_id <=", value, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdIn(List<Integer> values) {
			addCriterion("order_id in", values, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdNotIn(List<Integer> values) {
			addCriterion("order_id not in", values, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdBetween(Integer value1, Integer value2) {
			addCriterion("order_id between", value1, value2, "orderId");
			return (Criteria) this;
		}

		public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
			addCriterion("order_id not between", value1, value2, "orderId");
			return (Criteria) this;
		}

		public Criteria andSkuIsNull() {
			addCriterion("sku is null");
			return (Criteria) this;
		}

		public Criteria andSkuIsNotNull() {
			addCriterion("sku is not null");
			return (Criteria) this;
		}

		public Criteria andSkuEqualTo(String value) {
			addCriterion("sku =", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotEqualTo(String value) {
			addCriterion("sku <>", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuGreaterThan(String value) {
			addCriterion("sku >", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuGreaterThanOrEqualTo(String value) {
			addCriterion("sku >=", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuLessThan(String value) {
			addCriterion("sku <", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuLessThanOrEqualTo(String value) {
			addCriterion("sku <=", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuLike(String value) {
			addCriterion("sku like", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotLike(String value) {
			addCriterion("sku not like", value, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuIn(List<String> values) {
			addCriterion("sku in", values, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotIn(List<String> values) {
			addCriterion("sku not in", values, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuBetween(String value1, String value2) {
			addCriterion("sku between", value1, value2, "sku");
			return (Criteria) this;
		}

		public Criteria andSkuNotBetween(String value1, String value2) {
			addCriterion("sku not between", value1, value2, "sku");
			return (Criteria) this;
		}

		public Criteria andItemIdIsNull() {
			addCriterion("item_id is null");
			return (Criteria) this;
		}

		public Criteria andItemIdIsNotNull() {
			addCriterion("item_id is not null");
			return (Criteria) this;
		}

		public Criteria andItemIdEqualTo(Integer value) {
			addCriterion("item_id =", value, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdNotEqualTo(Integer value) {
			addCriterion("item_id <>", value, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdGreaterThan(Integer value) {
			addCriterion("item_id >", value, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("item_id >=", value, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdLessThan(Integer value) {
			addCriterion("item_id <", value, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdLessThanOrEqualTo(Integer value) {
			addCriterion("item_id <=", value, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdIn(List<Integer> values) {
			addCriterion("item_id in", values, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdNotIn(List<Integer> values) {
			addCriterion("item_id not in", values, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdBetween(Integer value1, Integer value2) {
			addCriterion("item_id between", value1, value2, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemIdNotBetween(Integer value1, Integer value2) {
			addCriterion("item_id not between", value1, value2, "itemId");
			return (Criteria) this;
		}

		public Criteria andItemNameIsNull() {
			addCriterion("item_name is null");
			return (Criteria) this;
		}

		public Criteria andItemNameIsNotNull() {
			addCriterion("item_name is not null");
			return (Criteria) this;
		}

		public Criteria andItemNameEqualTo(String value) {
			addCriterion("item_name =", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameNotEqualTo(String value) {
			addCriterion("item_name <>", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameGreaterThan(String value) {
			addCriterion("item_name >", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameGreaterThanOrEqualTo(String value) {
			addCriterion("item_name >=", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameLessThan(String value) {
			addCriterion("item_name <", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameLessThanOrEqualTo(String value) {
			addCriterion("item_name <=", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameLike(String value) {
			addCriterion("item_name like", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameNotLike(String value) {
			addCriterion("item_name not like", value, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameIn(List<String> values) {
			addCriterion("item_name in", values, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameNotIn(List<String> values) {
			addCriterion("item_name not in", values, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameBetween(String value1, String value2) {
			addCriterion("item_name between", value1, value2, "itemName");
			return (Criteria) this;
		}

		public Criteria andItemNameNotBetween(String value1, String value2) {
			addCriterion("item_name not between", value1, value2, "itemName");
			return (Criteria) this;
		}

		public Criteria andWeightIsNull() {
			addCriterion("weight is null");
			return (Criteria) this;
		}

		public Criteria andWeightIsNotNull() {
			addCriterion("weight is not null");
			return (Criteria) this;
		}

		public Criteria andWeightEqualTo(BigDecimal value) {
			addCriterion("weight =", value, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightNotEqualTo(BigDecimal value) {
			addCriterion("weight <>", value, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightGreaterThan(BigDecimal value) {
			addCriterion("weight >", value, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("weight >=", value, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightLessThan(BigDecimal value) {
			addCriterion("weight <", value, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightLessThanOrEqualTo(BigDecimal value) {
			addCriterion("weight <=", value, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightIn(List<BigDecimal> values) {
			addCriterion("weight in", values, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightNotIn(List<BigDecimal> values) {
			addCriterion("weight not in", values, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("weight between", value1, value2, "weight");
			return (Criteria) this;
		}

		public Criteria andWeightNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("weight not between", value1, value2, "weight");
			return (Criteria) this;
		}

		public Criteria andQuantityIsNull() {
			addCriterion("quantity is null");
			return (Criteria) this;
		}

		public Criteria andQuantityIsNotNull() {
			addCriterion("quantity is not null");
			return (Criteria) this;
		}

		public Criteria andQuantityEqualTo(Integer value) {
			addCriterion("quantity =", value, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityNotEqualTo(Integer value) {
			addCriterion("quantity <>", value, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityGreaterThan(Integer value) {
			addCriterion("quantity >", value, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityGreaterThanOrEqualTo(Integer value) {
			addCriterion("quantity >=", value, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityLessThan(Integer value) {
			addCriterion("quantity <", value, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityLessThanOrEqualTo(Integer value) {
			addCriterion("quantity <=", value, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityIn(List<Integer> values) {
			addCriterion("quantity in", values, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityNotIn(List<Integer> values) {
			addCriterion("quantity not in", values, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityBetween(Integer value1, Integer value2) {
			addCriterion("quantity between", value1, value2, "quantity");
			return (Criteria) this;
		}

		public Criteria andQuantityNotBetween(Integer value1, Integer value2) {
			addCriterion("quantity not between", value1, value2, "quantity");
			return (Criteria) this;
		}

		public Criteria andUomIdIsNull() {
			addCriterion("uom_id is null");
			return (Criteria) this;
		}

		public Criteria andUomIdIsNotNull() {
			addCriterion("uom_id is not null");
			return (Criteria) this;
		}

		public Criteria andUomIdEqualTo(Integer value) {
			addCriterion("uom_id =", value, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdNotEqualTo(Integer value) {
			addCriterion("uom_id <>", value, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdGreaterThan(Integer value) {
			addCriterion("uom_id >", value, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("uom_id >=", value, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdLessThan(Integer value) {
			addCriterion("uom_id <", value, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdLessThanOrEqualTo(Integer value) {
			addCriterion("uom_id <=", value, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdIn(List<Integer> values) {
			addCriterion("uom_id in", values, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdNotIn(List<Integer> values) {
			addCriterion("uom_id not in", values, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdBetween(Integer value1, Integer value2) {
			addCriterion("uom_id between", value1, value2, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomIdNotBetween(Integer value1, Integer value2) {
			addCriterion("uom_id not between", value1, value2, "uomId");
			return (Criteria) this;
		}

		public Criteria andUomValueIsNull() {
			addCriterion("uom_value is null");
			return (Criteria) this;
		}

		public Criteria andUomValueIsNotNull() {
			addCriterion("uom_value is not null");
			return (Criteria) this;
		}

		public Criteria andUomValueEqualTo(String value) {
			addCriterion("uom_value =", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueNotEqualTo(String value) {
			addCriterion("uom_value <>", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueGreaterThan(String value) {
			addCriterion("uom_value >", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueGreaterThanOrEqualTo(String value) {
			addCriterion("uom_value >=", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueLessThan(String value) {
			addCriterion("uom_value <", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueLessThanOrEqualTo(String value) {
			addCriterion("uom_value <=", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueLike(String value) {
			addCriterion("uom_value like", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueNotLike(String value) {
			addCriterion("uom_value not like", value, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueIn(List<String> values) {
			addCriterion("uom_value in", values, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueNotIn(List<String> values) {
			addCriterion("uom_value not in", values, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueBetween(String value1, String value2) {
			addCriterion("uom_value between", value1, value2, "uomValue");
			return (Criteria) this;
		}

		public Criteria andUomValueNotBetween(String value1, String value2) {
			addCriterion("uom_value not between", value1, value2, "uomValue");
			return (Criteria) this;
		}

		public Criteria andMrpPriceIsNull() {
			addCriterion("mrp_price is null");
			return (Criteria) this;
		}

		public Criteria andMrpPriceIsNotNull() {
			addCriterion("mrp_price is not null");
			return (Criteria) this;
		}

		public Criteria andMrpPriceEqualTo(BigDecimal value) {
			addCriterion("mrp_price =", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceNotEqualTo(BigDecimal value) {
			addCriterion("mrp_price <>", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceGreaterThan(BigDecimal value) {
			addCriterion("mrp_price >", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("mrp_price >=", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceLessThan(BigDecimal value) {
			addCriterion("mrp_price <", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("mrp_price <=", value, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceIn(List<BigDecimal> values) {
			addCriterion("mrp_price in", values, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceNotIn(List<BigDecimal> values) {
			addCriterion("mrp_price not in", values, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("mrp_price between", value1, value2, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andMrpPriceNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("mrp_price not between", value1, value2, "mrpPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceIsNull() {
			addCriterion("selling_price is null");
			return (Criteria) this;
		}

		public Criteria andSellingPriceIsNotNull() {
			addCriterion("selling_price is not null");
			return (Criteria) this;
		}

		public Criteria andSellingPriceEqualTo(BigDecimal value) {
			addCriterion("selling_price =", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceNotEqualTo(BigDecimal value) {
			addCriterion("selling_price <>", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceGreaterThan(BigDecimal value) {
			addCriterion("selling_price >", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("selling_price >=", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceLessThan(BigDecimal value) {
			addCriterion("selling_price <", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("selling_price <=", value, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceIn(List<BigDecimal> values) {
			addCriterion("selling_price in", values, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceNotIn(List<BigDecimal> values) {
			addCriterion("selling_price not in", values, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("selling_price between", value1, value2, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andSellingPriceNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("selling_price not between", value1, value2, "sellingPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceIsNull() {
			addCriterion("discount_price is null");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceIsNotNull() {
			addCriterion("discount_price is not null");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceEqualTo(BigDecimal value) {
			addCriterion("discount_price =", value, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceNotEqualTo(BigDecimal value) {
			addCriterion("discount_price <>", value, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceGreaterThan(BigDecimal value) {
			addCriterion("discount_price >", value, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("discount_price >=", value, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceLessThan(BigDecimal value) {
			addCriterion("discount_price <", value, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("discount_price <=", value, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceIn(List<BigDecimal> values) {
			addCriterion("discount_price in", values, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceNotIn(List<BigDecimal> values) {
			addCriterion("discount_price not in", values, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("discount_price between", value1, value2, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andDiscountPriceNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("discount_price not between", value1, value2, "discountPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceIsNull() {
			addCriterion("total_price is null");
			return (Criteria) this;
		}

		public Criteria andTotalPriceIsNotNull() {
			addCriterion("total_price is not null");
			return (Criteria) this;
		}

		public Criteria andTotalPriceEqualTo(BigDecimal value) {
			addCriterion("total_price =", value, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceNotEqualTo(BigDecimal value) {
			addCriterion("total_price <>", value, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceGreaterThan(BigDecimal value) {
			addCriterion("total_price >", value, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("total_price >=", value, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceLessThan(BigDecimal value) {
			addCriterion("total_price <", value, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceLessThanOrEqualTo(BigDecimal value) {
			addCriterion("total_price <=", value, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceIn(List<BigDecimal> values) {
			addCriterion("total_price in", values, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceNotIn(List<BigDecimal> values) {
			addCriterion("total_price not in", values, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("total_price between", value1, value2, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andTotalPriceNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("total_price not between", value1, value2, "totalPrice");
			return (Criteria) this;
		}

		public Criteria andCreatedbyIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedbyIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedbyEqualTo(Integer value) {
			addCriterion("created_by =", value, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyNotEqualTo(Integer value) {
			addCriterion("created_by <>", value, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyGreaterThan(Integer value) {
			addCriterion("created_by >", value, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyGreaterThanOrEqualTo(Integer value) {
			addCriterion("created_by >=", value, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyLessThan(Integer value) {
			addCriterion("created_by <", value, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyLessThanOrEqualTo(Integer value) {
			addCriterion("created_by <=", value, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyIn(List<Integer> values) {
			addCriterion("created_by in", values, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyNotIn(List<Integer> values) {
			addCriterion("created_by not in", values, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyBetween(Integer value1, Integer value2) {
			addCriterion("created_by between", value1, value2, "createdby");
			return (Criteria) this;
		}

		public Criteria andCreatedbyNotBetween(Integer value1, Integer value2) {
			addCriterion("created_by not between", value1, value2, "createdby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyIsNull() {
			addCriterion("modified_by is null");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyIsNotNull() {
			addCriterion("modified_by is not null");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyEqualTo(Integer value) {
			addCriterion("modified_by =", value, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyNotEqualTo(Integer value) {
			addCriterion("modified_by <>", value, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyGreaterThan(Integer value) {
			addCriterion("modified_by >", value, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyGreaterThanOrEqualTo(Integer value) {
			addCriterion("modified_by >=", value, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyLessThan(Integer value) {
			addCriterion("modified_by <", value, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyLessThanOrEqualTo(Integer value) {
			addCriterion("modified_by <=", value, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyIn(List<Integer> values) {
			addCriterion("modified_by in", values, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyNotIn(List<Integer> values) {
			addCriterion("modified_by not in", values, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyBetween(Integer value1, Integer value2) {
			addCriterion("modified_by between", value1, value2, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedbyNotBetween(Integer value1, Integer value2) {
			addCriterion("modified_by not between", value1, value2, "lastmodifiedby");
			return (Criteria) this;
		}

		public Criteria andCreatedonIsNull() {
			addCriterion("created_on is null");
			return (Criteria) this;
		}

		public Criteria andCreatedonIsNotNull() {
			addCriterion("created_on is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedonEqualTo(Date value) {
			addCriterion("created_on =", value, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonNotEqualTo(Date value) {
			addCriterion("created_on <>", value, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonGreaterThan(Date value) {
			addCriterion("created_on >", value, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonGreaterThanOrEqualTo(Date value) {
			addCriterion("created_on >=", value, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonLessThan(Date value) {
			addCriterion("created_on <", value, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonLessThanOrEqualTo(Date value) {
			addCriterion("created_on <=", value, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonIn(List<Date> values) {
			addCriterion("created_on in", values, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonNotIn(List<Date> values) {
			addCriterion("created_on not in", values, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonBetween(Date value1, Date value2) {
			addCriterion("created_on between", value1, value2, "createdon");
			return (Criteria) this;
		}

		public Criteria andCreatedonNotBetween(Date value1, Date value2) {
			addCriterion("created_on not between", value1, value2, "createdon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonIsNull() {
			addCriterion("lastmodifiedon is null");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonIsNotNull() {
			addCriterion("lastmodifiedon is not null");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonEqualTo(Date value) {
			addCriterion("lastmodifiedon =", value, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonNotEqualTo(Date value) {
			addCriterion("lastmodifiedon <>", value, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonGreaterThan(Date value) {
			addCriterion("lastmodifiedon >", value, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonGreaterThanOrEqualTo(Date value) {
			addCriterion("lastmodifiedon >=", value, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonLessThan(Date value) {
			addCriterion("lastmodifiedon <", value, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonLessThanOrEqualTo(Date value) {
			addCriterion("lastmodifiedon <=", value, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonIn(List<Date> values) {
			addCriterion("lastmodifiedon in", values, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonNotIn(List<Date> values) {
			addCriterion("lastmodifiedon not in", values, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonBetween(Date value1, Date value2) {
			addCriterion("lastmodifiedon between", value1, value2, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andLastmodifiedonNotBetween(Date value1, Date value2) {
			addCriterion("lastmodifiedon not between", value1, value2, "lastmodifiedon");
			return (Criteria) this;
		}

		public Criteria andIsActiveIsNull() {
			addCriterion("is_active is null");
			return (Criteria) this;
		}

		public Criteria andIsActiveIsNotNull() {
			addCriterion("is_active is not null");
			return (Criteria) this;
		}

		public Criteria andIsActiveEqualTo(Boolean value) {
			addCriterion("is_active =", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotEqualTo(Boolean value) {
			addCriterion("is_active <>", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveGreaterThan(Boolean value) {
			addCriterion("is_active >", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveGreaterThanOrEqualTo(Boolean value) {
			addCriterion("is_active >=", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveLessThan(Boolean value) {
			addCriterion("is_active <", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveLessThanOrEqualTo(Boolean value) {
			addCriterion("is_active <=", value, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveIn(List<Boolean> values) {
			addCriterion("is_active in", values, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotIn(List<Boolean> values) {
			addCriterion("is_active not in", values, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveBetween(Boolean value1, Boolean value2) {
			addCriterion("is_active between", value1, value2, "isActive");
			return (Criteria) this;
		}

		public Criteria andIsActiveNotBetween(Boolean value1, Boolean value2) {
			addCriterion("is_active not between", value1, value2, "isActive");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_order_details
	 * @mbggenerated
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table tbl_order_details
     *
     * @mbggenerated do_not_delete_during_merge
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}