package co.stutzen.goodwill.entity;

import java.util.Date;

public class UOM {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.id
	 * @mbggenerated
	 */
	private Integer id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.name
	 * @mbggenerated
	 */
	private String name;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.created_by
	 * @mbggenerated
	 */
	private Integer createdby;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.modified_by
	 * @mbggenerated
	 */
	private Integer lastmodifiedby;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.created_on
	 * @mbggenerated
	 */
	private Date createdon;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.lastmodifiedon
	 * @mbggenerated
	 */
	private Date lastmodifiedon;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.is_active
	 * @mbggenerated
	 */
	private Boolean isActive;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_uom.comments
	 * @mbggenerated
	 */
	private String comments;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.id
	 * @return  the value of tbl_uom.id
	 * @mbggenerated
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.id
	 * @param id  the value for tbl_uom.id
	 * @mbggenerated
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.name
	 * @return  the value of tbl_uom.name
	 * @mbggenerated
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.name
	 * @param name  the value for tbl_uom.name
	 * @mbggenerated
	 */
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.created_by
	 * @return  the value of tbl_uom.created_by
	 * @mbggenerated
	 */
	public Integer getCreatedby() {
		return createdby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.created_by
	 * @param createdby  the value for tbl_uom.created_by
	 * @mbggenerated
	 */
	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.modified_by
	 * @return  the value of tbl_uom.modified_by
	 * @mbggenerated
	 */
	public Integer getLastmodifiedby() {
		return lastmodifiedby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.modified_by
	 * @param lastmodifiedby  the value for tbl_uom.modified_by
	 * @mbggenerated
	 */
	public void setLastmodifiedby(Integer lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.created_on
	 * @return  the value of tbl_uom.created_on
	 * @mbggenerated
	 */
	public Date getCreatedon() {
		return createdon;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.created_on
	 * @param createdon  the value for tbl_uom.created_on
	 * @mbggenerated
	 */
	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.lastmodifiedon
	 * @return  the value of tbl_uom.lastmodifiedon
	 * @mbggenerated
	 */
	public Date getLastmodifiedon() {
		return lastmodifiedon;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.lastmodifiedon
	 * @param lastmodifiedon  the value for tbl_uom.lastmodifiedon
	 * @mbggenerated
	 */
	public void setLastmodifiedon(Date lastmodifiedon) {
		this.lastmodifiedon = lastmodifiedon;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.is_active
	 * @return  the value of tbl_uom.is_active
	 * @mbggenerated
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.is_active
	 * @param isActive  the value for tbl_uom.is_active
	 * @mbggenerated
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_uom.comments
	 * @return  the value of tbl_uom.comments
	 * @mbggenerated
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_uom.comments
	 * @param comments  the value for tbl_uom.comments
	 * @mbggenerated
	 */
	public void setComments(String comments) {
		this.comments = comments == null ? null : comments.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_uom
	 * @mbggenerated
	 */
	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UOM other = (UOM) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
				&& (this.getCreatedby() == null ? other.getCreatedby() == null
						: this.getCreatedby().equals(other.getCreatedby()))
				&& (this.getLastmodifiedby() == null ? other.getLastmodifiedby() == null
						: this.getLastmodifiedby().equals(other.getLastmodifiedby()))
				&& (this.getCreatedon() == null ? other.getCreatedon() == null
						: this.getCreatedon().equals(other.getCreatedon()))
				&& (this.getLastmodifiedon() == null ? other.getLastmodifiedon() == null
						: this.getLastmodifiedon().equals(other.getLastmodifiedon()))
				&& (this.getIsActive() == null ? other.getIsActive() == null
						: this.getIsActive().equals(other.getIsActive()))
				&& (this.getComments() == null ? other.getComments() == null
						: this.getComments().equals(other.getComments()));
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_uom
	 * @mbggenerated
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getCreatedby() == null) ? 0 : getCreatedby().hashCode());
		result = prime * result + ((getLastmodifiedby() == null) ? 0 : getLastmodifiedby().hashCode());
		result = prime * result + ((getCreatedon() == null) ? 0 : getCreatedon().hashCode());
		result = prime * result + ((getLastmodifiedon() == null) ? 0 : getLastmodifiedon().hashCode());
		result = prime * result + ((getIsActive() == null) ? 0 : getIsActive().hashCode());
		result = prime * result + ((getComments() == null) ? 0 : getComments().hashCode());
		return result;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_uom
	 * @mbggenerated
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", name=").append(name);
		sb.append(", createdby=").append(createdby);
		sb.append(", lastmodifiedby=").append(lastmodifiedby);
		sb.append(", createdon=").append(createdon);
		sb.append(", lastmodifiedon=").append(lastmodifiedon);
		sb.append(", isActive=").append(isActive);
		sb.append(", comments=").append(comments);
		sb.append("]");
		return sb.toString();
	}
}