package co.stutzen.goodwill.service.impl;

import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.goodwill.entity.UOM;
import co.stutzen.goodwill.service.UOMService;

@Service
public class UOMServiceImpl extends SWFServiceImpl<UOM> implements UOMService {

}
