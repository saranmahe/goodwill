package co.stutzen.goodwill.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.goodwill.entity.Contact;

public interface ContactService extends SWFService<Contact> {

}
