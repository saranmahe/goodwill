package co.stutzen.goodwill.service.impl;

import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.goodwill.entity.Item;
import co.stutzen.goodwill.service.ItemService;

@Service
public class ItemServiceImpl extends SWFServiceImpl<Item> implements ItemService {

}
