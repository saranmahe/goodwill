package co.stutzen.goodwill.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.goodwill.entity.Item;

public interface ItemService extends SWFService<Item> {

}
