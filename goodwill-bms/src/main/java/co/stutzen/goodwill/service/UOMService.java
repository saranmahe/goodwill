package co.stutzen.goodwill.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.goodwill.entity.UOM;

public interface UOMService extends SWFService<UOM> {

}
