package co.stutzen.goodwill.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.goodwill.entity.Notes;

public interface NotesService extends SWFService<Notes> {

}
