package co.stutzen.goodwill.service.impl;

import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.goodwill.entity.Notes;
import co.stutzen.goodwill.service.NotesService;

@Service
public class NotesServiceImpl extends SWFServiceImpl<Notes> implements NotesService {

}
