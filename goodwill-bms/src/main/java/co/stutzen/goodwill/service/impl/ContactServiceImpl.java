package co.stutzen.goodwill.service.impl;

import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.goodwill.entity.Contact;
import co.stutzen.goodwill.service.ContactService;

@Service
public class ContactServiceImpl extends SWFServiceImpl<Contact> implements ContactService {

}
