package co.stutzen.goodwill.service;

import org.stutzen.webframework.service.SWFService;

import co.stutzen.goodwill.entity.User;

public interface UserService extends SWFService<User> {

}
