package co.stutzen.goodwill.service.impl;

import org.springframework.stereotype.Service;
import org.stutzen.webframework.service.impl.SWFServiceImpl;

import co.stutzen.goodwill.entity.User;
import co.stutzen.goodwill.service.UserService;

@Service
public class UserServiceImpl extends SWFServiceImpl<User> implements UserService {

}
